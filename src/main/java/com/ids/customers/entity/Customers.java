package com.ids.customers.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "customers")
@Data
public class Customers {
    @Id
    @Column(nullable = false)
    private String customerNumber;
    private String name;
    private String lastName;
    private boolean session;
    private String password;
    private String email;
    private String status;
    private String creation;
}
