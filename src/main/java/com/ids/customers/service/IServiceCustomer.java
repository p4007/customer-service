package com.ids.customers.service;

import org.springframework.http.ResponseEntity;

public interface IServiceCustomer {
    String validateCustomer(String user, String password);
}
