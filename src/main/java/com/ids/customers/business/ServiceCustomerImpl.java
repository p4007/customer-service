package com.ids.customers.business;

import com.ids.customers.entity.Customers;
import com.ids.customers.repository.ICustomers;
import com.ids.customers.service.IServiceCustomer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class ServiceCustomerImpl implements IServiceCustomer {

    @Autowired
    private ICustomers iCustomers;

    @Override
    public String validateCustomer(String user, String password) {
        Customers customers = iCustomers.findCustomersByPasswordANDEmail(user, password);
        if(customers != null){
            customers.setSession(true);
            iCustomers.save(customers);
            return UUID.randomUUID().toString();
        }
        return null;
    }
}
