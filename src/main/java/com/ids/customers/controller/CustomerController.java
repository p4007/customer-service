package com.ids.customers.controller;

import com.ids.customers.service.IServiceCustomer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api")
public class CustomerController {

    @Autowired
    private IServiceCustomer iServiceCustomer;

    @RequestMapping(value = "/validate")
    public ResponseEntity validateCredentials(@RequestParam String user, @RequestParam String password){
        System.out.println("Haciendo consulta del usuario: " + user);
        String uuid = iServiceCustomer.validateCustomer(user, password);
        if(uuid != null){
            System.out.println("Usuario autorizado...");
            return ResponseEntity.status(200).header("autorization", uuid).build();
        }
        System.out.println("Usuario no autorizado...");
        return ResponseEntity.status(400).build();
    }
}
