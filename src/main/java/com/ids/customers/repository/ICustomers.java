package com.ids.customers.repository;

import com.ids.customers.entity.Customers;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ICustomers extends JpaRepository<Customers, String> {
    @Query(value = "SELECT c FROM customers c WHERE c.email = :email AND c.password = :password AND status = 'A'")
    Customers findCustomersByPasswordANDEmail(@Param("email") String user, @Param("password") String password);
}

